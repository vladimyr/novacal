'use strict';

const cheerio = require('cheerio');
const client = require('got').extend({
  baseUrl: 'https://www.novasol.com/',
  headers: {
    'cache-control': 'no-cache',
    pragma: 'no-cache'
  }
});

const getGeoCoordinates = info => info.rentalAddress.coordinates;

module.exports = {
  getCalendarData,
  getInfo,
  getBookings,
  getGeoCoordinates
};

function getCalendarData(propertyId) {
  const query = { propertyId, loadAvailability: 1, t: (new Date()).getTime() };
  return client.get('/novasol/getPrice', { query }).then(resp => JSON.parse(resp.body));
}

async function getInfo(propertyId) {
  const { body } = await client.get(`/p/${propertyId}`);
  const $ = cheerio.load(body);
  const json = $('script[type="application/json"]').html();
  return JSON.parse(json).solar_ro;
}

async function getBookings(propertyId) {
  const { datesRanges: availableRanges } = await getCalendarData(propertyId);
  const flatRanges = getFlatRanges(availableRanges);
  const bookings = flatRanges.reduce((acc, current) => {
    const last = acc[acc.length - 1];
    if (!last || last.end) {
      acc.push({ start: parseDate(current.to) });
      return acc;
    }
    last.end = parseDate(current.from);
    acc.push({ start: parseDate(current.to) });
    return acc;
  }, []);
  bookings.pop();
  return bookings;
}

function parseDate(str) {
  const year = str.substr(0, 4);
  const month = str.substr(4, 2);
  const day = str.substr(6, 2);
  return `${year}-${month}-${day}`;
}

function getFlatRanges(availableRanges = []) {
  availableRanges = availableRanges.sort((prev, next) => {
    return prev.from === next.from
      ? prev.to - next.to
      : prev.from - next.from;
  });
  return availableRanges.reduce((acc, current) => {
    const last = acc[acc.length - 1];
    if (!last || last.to < current.from) {
      acc.push(current);
      return acc;
    }
    if (last.to < current.to) {
      last.to = current.to;
      return acc;
    }
    return acc;
  }, []);
}
