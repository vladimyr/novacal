'use strict';

const client = require('got').extend({
  baseUrl: 'https://www.tripadvisor.com/data/1.0/vr/',
  headers: {
    'cache-control': 'no-cache',
    pragma: 'no-cache'
  }
});

const formatDate = date => date.toISOString().split('T')[0];

module.exports = {
  getCalendarData,
  getInfo,
  getBookings,
  getGeoCoordinates
};

async function getCalendarData(propertyId) {
  const resp = await client.get(`/rental/calendarData/${propertyId}`, { json: true });
  return resp.body;
}

async function getInfo(propertyId) {
  const resp = await client.get(`/rental/${propertyId}`, { json: true });
  return resp.body;
}

function getGeoCoordinates({ geoCoordinate }) {
  return { latitude: geoCoordinate.lat, longitude: geoCoordinate.lon };
}

async function getBookings(propertyId) {
  const { bookedBits, bookedRanges, ...data } = await getCalendarData(propertyId);
  if (Array.isArray(bookedRanges)) {
    return bookedRanges.map(({ start, end }) => ({
      start: formatDate(new Date(start)),
      end: formatDate(new Date(end))
    }));
  }
  const ranges = getRanges(bookedBits);
  const startDate = parseDate(data.startDate);
  return ranges.reduce((acc, offset, index) => {
    if (index % 2 === 0) {
      const start = formatDate(addDays(startDate, offset));
      acc.push({ start });
      return acc;
    }
    const last = acc[acc.length - 1];
    const end = formatDate(addDays(startDate, offset));
    last.end = end;
    return acc;
  }, []);
}

function addDays(date, days) {
  const result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
}

function parseDate(str) {
  const [year, month, day] = str.split('/');
  return new Date(year, month, day);
}

function getRanges(bookedBits = {}) {
  const bookedDays = Object.keys(bookedBits)
    .reduce((acc, index) => {
      if (!bookedBits[index]) return acc;
      acc.push(parseInt(index, 10));
      return acc;
    }, [])
    .sort((a, b) => a - b);
  const ranges = bookedDays.reduce((acc, current, index, arr) => {
    if (index === 0) {
      acc.push(current);
      return acc;
    }
    const prev = arr[index - 1];
    if (current - prev > 1) acc.push(prev, current);
    return acc;
  }, []);
  ranges.push(bookedDays[bookedDays.length - 1]);
  return ranges;
}
