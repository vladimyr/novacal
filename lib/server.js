'use strict';

exports.sendCalendar = function (res, propertyId, calendar) {
  res.setHeader('Content-Type', 'text/calendar; charset=UTF-8');
  res.setHeader('Content-Disposition', `attachment; filename="${propertyId}-novasol.ics"`);
  res.end(calendar);
};

exports.sendJson = sendJson;
function sendJson(res, data, { statusCode = 200 } = {}) {
  res.setHeader('Content-Type', 'application/json; charset=UTF-8');
  res.statusCode = statusCode;
  res.end(JSON.stringify(data));
}

const jsend = exports.jsend = {};
jsend.success = function (res, data, { statusCode = 200 } = {}) {
  return sendJson(res, { status: 'success', data }, { statusCode });
};

jsend.fail = function (res, data, { statusCode = 400 } = {}) {
  return sendJson(res, { status: 'fail', data }, { statusCode });
};

jsend.error = function (res, message, { statusCode = 500 } = {}) {
  return sendJson(res, { status: 'error', message }, { statusCode });
};

exports.nocache = function (res) {
  res.setHeader('Surrogate-Control', 'no-store');
  res.setHeader('Cache-Control', 'no-store, no-cache, must-revalidate, proxy-revalidate');
  res.setHeader('Pragma', 'no-cache');
  res.setHeader('Expires', '0');
};
