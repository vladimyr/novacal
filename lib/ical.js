'use strict';

const { URL } = require('url');
// const domain = require('../now.json').alias;
const domain = 'novacal.now.sh';
const ical = require('ical-generator');
const tzLookup = require('tz-lookup');

const hours = num => num * 60 * 60;

module.exports = function createCalendar({ bookings, geoCoordinates, path }) {
  const timezone = getTimezone(geoCoordinates);
  const url = new URL(`https://${domain}`);
  url.pathname = path;
  const calendar = ical({
    domain,
    name: 'Bookings',
    url: url.href,
    ttl: hours(6)
  });
  bookings.forEach(({ start, end }) => calendar.createEvent({
    start: new Date(start),
    end: new Date(end),
    timezone
  }));
  return calendar.toString();
};

function getTimezone({ latitude, longitude } = {}) {
  try {
    return tzLookup(latitude, longitude);
  } catch (err) {
    return null;
  }
}
