'use strict';

const { jsend, nocache, sendCalendar } = require('./server');
const { HTTPError } = require('got');
const { URLSearchParams } = require('url');
const createCalendar = require('./ical');
const pupa = require('pupa');

const getSearchParams = url => new URLSearchParams(url.split('?')[1]);
const isCalendar = format => ['ics', 'ical'].includes(format);
const isNotFound = err => err instanceof HTTPError && err.statusCode === 404;

module.exports = ({ route, service }) => async (req, res) => {
  nocache(res);
  const searchParams = getSearchParams(req.url);
  const format = searchParams.get('format') || 'json';
  if (format !== 'json' && !isCalendar(format)) {
    return jsend.fail(res, { format: `Unsupported \`format\`: ${format}` });
  }
  const propertyId = searchParams.get('property');
  try {
    const [bookings, info] = await Promise.all([
      service.getBookings(propertyId),
      service.getInfo(propertyId)
    ]);
    if (!isCalendar(format)) return jsend.success(res, { info, bookings });
    const geoCoordinates = service.getGeoCoordinates(info);
    const path = pupa(route, { propertyId });
    const calendar = createCalendar({ bookings, geoCoordinates, path });
    return sendCalendar(res, propertyId, calendar);
  } catch (err) {
    if (isNotFound(err)) {
      const data = { propertyId: `Invalid \`propertyId\` provided: ${propertyId}` };
      return jsend.fail(res, data, { statusCode: err.statusCode });
    }
    return jsend.error(res, 'Oops! Something went wrong.');
  }
};
