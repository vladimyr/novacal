'use strict';

const { simulator } = require('now-we-test');
const test = require('tape');

testSuite('novasol', require('./api/novasol'), { property: 'CDC198' });
testSuite('tripadvisor', require('./api/tripadvisor'), { property: '16650229' });

function testSuite(name, func, { property }) {
  const handler = simulator(func);

  test(`[${name}] fails on invalid \`propertyId\` provided`, async t => {
    const resp = await handler.get('/').query({ property: 'dummy' });
    t.comment(`status=${resp.status}`);
    t.plan(3);
    t.equals(resp.type, 'application/json', 'response type is json');
    t.ok(resp.notFound, 'status code is 404');
    const result = JSON.parse(resp.text);
    const data = result && result.data;
    t.equals(
      data && data.propertyId,
      'Invalid `propertyId` provided: dummy',
      '`data.propertyId` contains failure description'
    );
  });

  test(`[${name}] fails on unsupported \`format\``, async t => {
    const resp = await handler.get('/').query({ format: 'dummy' });
    t.comment(`status=${resp.status}`);
    t.plan(3);
    t.equals(resp.type, 'application/json', 'response type is json');
    t.ok(resp.badRequest, 'status code is 400');
    const result = JSON.parse(resp.text);
    const data = result && result.data;
    t.equals(
      data && data.format,
      'Unsupported `format`: dummy',
      '`data.format` contains failure description'
    );
  });

  test(`[${name}] returns bookings in \`json\` format`, async t => {
    const resp = await handler.get('/').query({ property });
    t.comment(`status=${resp.status}`);
    t.plan(3);
    t.equals(resp.type, 'application/json', 'response type is json');
    t.ok(resp.ok, 'status code is 200');
    const result = JSON.parse(resp.text);
    const data = result && result.data;
    t.ok(Array.isArray(data.bookings), '`data` contains array of bookings');
  });
}
