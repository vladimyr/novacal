# novacal [![js semistandard style](https://badgen.net/badge/code%20style/semistandard/pink)](https://github.com/Flet/semistandard)

# Usage

```sh
# grab booking calendar
$ curl -s https://novcal.now.sh/novasol/p/<property_id>.ics
# use `.json` for JSON dump
$ curl -s https://novcal.now.sh/novasol/p/<property_id>.json
```
