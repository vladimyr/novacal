'use strict';

module.exports = require('../lib/lambda')({
  route: '/tripadvisor/{propertyId}.ics',
  service: require('../service/tripadvisor')
});
