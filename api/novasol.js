'use strict';

module.exports = require('../lib/lambda')({
  route: '/novasol/p/{propertyId}.ics',
  service: require('../service/novasol')
});
